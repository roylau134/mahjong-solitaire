﻿using System;
using MemberManager.Controller;
using MemberManager.Module;

namespace MahjongSolitaire
{
    class Program
    {
        static void Main(string[] args)
        {
            MemberController memberController = new MemberController();
            Member memberRecord = null;
            string account = "roylau2";
            string password = "123456";
            if (memberController.IsRegisterSuccess(account, password, out memberRecord))
            {
                Console.WriteLine("Success");
            }
            else
            {
                Console.WriteLine("Is not correct");
            }
            PrintExitMessage();
        }

        static void PrintExitMessage()
        {
            Console.Write("Press any key to exit the program...");
            Console.ReadKey();
        }
    }
}
