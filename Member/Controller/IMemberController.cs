﻿using MemberManager.Module;

namespace MemberManager.Controller
{
    interface IMemberController
    {
        bool IsRegisterSuccess(string account, string password, out Member member);
        bool IsMemberExist(string account);
    }
}
