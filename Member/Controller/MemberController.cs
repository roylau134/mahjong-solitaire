﻿using MemberManager.AccessObject;
using MemberManager.Module;
using System.Collections.Generic;
using System.Linq;

namespace MemberManager.Controller
{
    public class MemberController : IMemberController
    {
        private static readonly MemberAO memberAO = new MemberAO();
        /// <summary>
        /// Name as Key
        /// </summary>
        private readonly Dictionary<string, Member> MemberDic;

        /// <summary>
        /// Controll the member record.
        /// </summary>
        public MemberController()
        {
            MemberDic = memberAO.Read();
            List<Member> memberList = MemberDic.Select(item => item.Value).ToList();
            memberAO.Init(memberList);
        }

        /// <summary>
        /// Register the user. If success, return true
        /// </summary>
        /// <param name="account">the user account</param>
        /// <param name="password">the user password</param>
        /// <param name="member">the result will be return on this object</param>
        /// <returns></returns>
        public bool IsRegisterSuccess(string account, string password, out Member member)
        {
            member = null;
            if (this.IsMemberExist(account))
            {
                return false;
            }
            else
            {
                member = CreateMember(account, password);
                return true;
            }
        }

        /// <summary>
        /// Check the member is exit by user account
        /// </summary>
        /// <param name="account">The account that want to check</param>
        /// <returns>True if found, otherwise false</returns>
        public bool IsMemberExist(string account)
        {
            return this.MemberDic.ContainsKey(account);
        }

        /// <summary>
        /// Create a member, will also add to the "Dictionary<string, Member> MemberDic"
        /// </summary>
        /// <param name="account">account name</param>
        /// <param name="password">raw password</param>
        /// <returns>new Member object</returns>
        private Member CreateMember(string account, string password)
        {
            Member result = new Member(account, password);
            MemberDic.Add(account, result);
            memberAO.Write(result);
            return result;
        }
    }
}
