﻿namespace MemberManager.Config
{
    /// <summary>
    /// Storing the config key
    /// </summary>
    internal class ConfigKey
    {
        /// <summary>
        /// The key of config. The path of where to store the Member Login Data
        /// </summary>
        public static readonly string MemberDataFilePathKey = "MemberDataFilePath";
    }
}
