﻿using System.Configuration;

namespace MemberManager.Config
{
    /// <summary>
    /// Help to control and read config section
    /// </summary>
    internal class ConfigController
    {
        /// <summary>
        /// The path of where to store the Member Login Data
        /// </summary>
        public static readonly string MemberDataFilePath = ConfigurationManager.AppSettings[ConfigKey.MemberDataFilePathKey];

        /// <summary>
        /// Help to control and read config section
        /// </summary>
        static ConfigController()
        {
        }
    }
}
