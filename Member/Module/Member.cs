﻿using MemberManager.Helper;

namespace MemberManager.Module
{
    public class Member
    {
        /// <summary>
        /// The name of the user
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// The password of the user, MD5-ed string
        /// </summary>
        public string Password { get; internal set; }

        /// <summary>
        /// Init a member
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password">raw password</param>
        public Member(string name, string password)
        {
            this.Name = name;
            this.Password = GetPassword(name, password);
        }

        /// <summary>
        /// REturn JSON String
        /// </summary>
        public string ToJSONString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Generate the password base on the input
        /// </summary>
        /// <param name="name"></param>
        /// <param name="rawPassword">the raw password</param>
        /// <returns>Hashed passowrd, use for login</returns>
        private string GetPassword(string name, string rawPassword)
        {
            string temp = string.Format("{0}.{1}", name, rawPassword);
            return HashHelper.ToSHA2(temp);
        }
    }
}
