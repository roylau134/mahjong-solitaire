﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MemberManager.Helper
{
    public static class HashHelper
    {
        public static string ToSHA2(string input)
        {
            SHA512 sha512 = new SHA512CryptoServiceProvider();
            string resultSha512 = Convert.ToBase64String(sha512.ComputeHash(Encoding.Default.GetBytes(input)));
            return resultSha512;
        }
    }
}
