﻿using System;
using System.IO;
using System.Text;

namespace MemberManager.AccessObject
{
    /// <summary>
    /// A base Access Object which handle text file
    /// </summary>
    internal class FileAO
    {
        internal string Path { get; set; }

        internal bool IsExist
        {
            get { return File.Exists(this.Path); }
        }

        internal FileAO(string path)
        {
            this.Path = path;
        }

        private string fileNotFoundMessage
        {
            get
            {
                return string.Format("File not found. Path \"{0}\"", this.Path);
            }
        }

        /// <summary>
        /// Read as string
        /// </summary>
        /// <returns>the raw string of the file data</returns>
        internal string Read()
        {
            if (this.IsExist)
            {
                StringBuilder result = new StringBuilder();
                String line;

                //Pass the file path and file name to the StreamReader constructor
                using (StreamReader sr = new StreamReader(this.Path))
                {

                    //Read the first line of text
                    line = sr.ReadLine();

                    //Continue to read until you reach end of file
                    while (line != null)
                    {
                        //write the lie to StringBuilder
                        result.AppendLine(line);
                        //Read the next line
                        line = sr.ReadLine();
                    }

                    //close the file
                    sr.Close();
                }
                return result.ToString();
            }
            throw new Exception(fileNotFoundMessage);
        }

        /// <summary>
        /// Overwrite the file in new line with the input
        /// </summary>
        /// <param name="data">raw string</param>
        internal void WriteLine(string data)
        {
            this.WriteData(data, true);
        }

        /// <summary>
        /// Overwrite the file with the input
        /// </summary>
        /// <param name="data">raw string</param>
        internal void Write(string data)
        {
            this.WriteData(data, false);
        }

        /// <summary>
        /// Overwrite the file with the input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isNewLine">if true, wite on new line. otherwise write on same line</param>
        private void WriteData(string data, bool isNewLine)
        {
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(this.Path))
            {
                if (isNewLine)
                {
                    sw.Close();
                }
                else
                {
                    sw.Write(data);
                }
                sw.Close();
            }
        }

        /// <summary>
        /// Empty the file. Same as creat new file.
        /// </summary>
        internal void Clear()
        {
            WriteLine(string.Empty);
        }

        /// <summary>
        /// Create an empty file
        /// </summary>
        internal void Create()
        {
            DirectoryInfo di = Directory.CreateDirectory(System.IO.Path.GetDirectoryName(this.Path));
            FileStream fs = File.Create(this.Path);
            fs.Close();
        }

        /// <summary>
        /// Append the text
        /// </summary>
        /// <param name="data">raw string</param>
        internal void Append(string data)
        {
            if (this.IsExist)
            {
                using (StreamWriter sw = File.AppendText(this.Path))
                {
                    sw.WriteLine(data);
                    sw.Close();
                }
                return;
            }
            throw new Exception(fileNotFoundMessage);
        }

        /// <summary>
        /// Append the text
        /// </summary>
        /// <param name="data">raw string</param>
        /// <param name="isNeedToCreateNew">If true, will create a new file first</param>
        internal void Append(string data, bool isNeedToCreateNew)
        {
            if (isNeedToCreateNew)
            {
                Create();
                this.Write(data);
            }
            else
            {
                this.Append(data);
            }
        }
    }
}