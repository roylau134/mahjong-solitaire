﻿using MemberManager.Module;
using System.Collections.Generic;

namespace MemberManager.AccessObject
{
    internal interface IMemberAO
    {
        /// <summary>
        /// Read the file
        /// </summary>
        /// <returns>Member Dictionary</returns>
        Dictionary<string, Member> Read();
        /// <summary>
        /// Append member record
        /// </summary>
        /// <param name="member">The member record</param>
        void Write(Member member);
        /// <summary>
        /// Append members record
        /// </summary>
        /// <param name="memberList">The members record</param>
        void Write(List<Member> memberList);
        /// <summary>
        /// Clear member record
        /// </summary>
        void Clear();
    }
}