﻿using MemberManager.Config;
using MemberManager.Module;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberManager.AccessObject
{
    internal class MemberAO : IMemberAO
    {
        internal static readonly string FilePath = ConfigController.MemberDataFilePath;
        private FileAO fileAO;

        /// <summary>
        /// Member Acces Object, use for handling the read and write operation between the file and object.
        /// </summary>
        public MemberAO()
        {
            fileAO = new FileAO(FilePath);
        }

        /// <summary>
        /// Read the Members Record
        /// </summary>
        /// <returns>Return a Dictionary<string: account, Member>/returns>
        public Dictionary<string, Member> Read()
        {
            Dictionary<string, Member> MemberDic = new Dictionary<string, Member>();
            try
            {
                string result = fileAO.Read();

                if (!string.IsNullOrEmpty(result))
                {
                    List<string> resultArray = result.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Where(eachLine => !string.IsNullOrEmpty(eachLine)).ToList();
                    foreach (string memberResult in resultArray)
                    {
                        try
                        {
                            Member member = JsonConvert.DeserializeObject<Member>(memberResult);
                            MemberDic.Add(member.Name, member);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }
            catch (Exception e)
            {
                // if error, init the file
                fileAO.Create();
            }
            return MemberDic;
        }

        /// <summary>
        /// Write a new member record to the file
        /// </summary>
        /// <param name="member">New member</param>
        public void Write(Member member)
        {
            fileAO.Append(member.ToJSONString());
        }

        /// <summary>
        /// Write list of Member records to the file 
        /// </summary>
        /// <param name="memberList">Lsit of Members</param>
        public void Write(List<Member> memberList)
        {
            fileAO.Append(ListMemberToString(memberList));
        }

        /// <summary>
        /// Init the file. Will clear the file then write to it.
        /// </summary>
        /// <param name="memberList"></param>
        public void Init(List<Member> memberList)
        {
            fileAO.Append(ListMemberToString(memberList), true);
        }

        /// <summary>
        /// Set the file as empty
        /// </summary>
        public void Clear()
        {
            fileAO.Clear();
        }

        /// <summary>
        /// Turn list of members to special string
        /// </summary>
        /// <param name="memberList"></param>
        /// <returns>special string</returns>
        private string ListMemberToString(List<Member> memberList)
        {
            StringBuilder result = new StringBuilder();
            memberList.ForEach(member =>
            {
                result.AppendLine(member.ToJSONString());
            });
            return result.ToString();
        }
    }
}